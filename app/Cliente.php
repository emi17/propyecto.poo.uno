<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
      "Nombre",
      "Apellido",
      "Cedula",
      "Direcciòn",
      "Telefono",
      "Fecha_nacimiento",
      "Email"
    ];
}

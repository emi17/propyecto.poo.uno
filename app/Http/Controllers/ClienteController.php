<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClienteController extends Controller
{

    /**
     * Mostrar la lista clientes.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function inicioCliente(Request $request)
    {
        dd("lista_cliente");
    }

    /**
     * Mostrar la funcion agregar cliente.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function agregarCliente(Request $request)
    {
        dd("agregar_cliente");
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('home');
    }

}

